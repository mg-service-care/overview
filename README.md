# Overview

This project is defined to maintain documentation about all other projects,
describing architecture and technologies envolving the Service Care On Road.

The instructions about the project are described in received
[documentation](man-truck-monitor.pdf).


![Web Sugestion](images/design-suggestion.png)

## Architecture

The project uses a database to store the trucker's localization. The data is
rescued by an BFF system/API that is required by a static frontend system. The
frontend renders the map, the localization the points of interest and all others
layers using google maps API.

```mermaid
graph LR;

subgraph internal
  Database;
end

subgraph public access
  Database --- API;
  API("BFF/Rest API") ---|http/ajax| frontend("Static FrontEnd");
end

subgraph external
  frontend("Static FrontEnd") ---|js SDK| GoogleAPI;
end
```

## Ecosystem

With the necessity of multiple domains, it's sugested to use various projects
where each one is responsible for each part of main project.

There is:

- `api` - a server side application that is responsible to give informations
  about the truck driver
- `frontend` - an application that renders the user interface, where the client
  can see all informations about location and POIs and help the truck driver

There's still a non mentioned database where are disposed information about the
truck driver.

With this scenario it's very hard to develop and maintain was developed an
`ecosystem` application where all the projects can run as one only.

```mermaid
graph LR;

subgraph ecosystem project
  subgraph internal
    Database;
  end

  subgraph public access
    Database --- API;
    API("BFF/Rest API") ---|http/ajax| frontend("Static FrontEnd");
  end
end

subgraph external
  frontend("Static FrontEnd") ---|js SDK| GoogleAPI;
end
```